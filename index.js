/*
	JavaScript Functions



	let firstName = `Dave`
	let lastName = `Supan`
	let age =	26
	let currentJob = `Web Developer` 
	let careerShift =  `To pursue interests`
	let futureGoals = `To become a tech lead`

	function helloThere() {
		console.log(`Hi I'm ${fistName} ${lastName}. I am ${age} years old. I work as a ${currentJob} because I want ${careerShift}. My future goal is ${futureGoals}.`)
	}

*/

/*
	If-Else Statement


	let subs = 300

	if (subs <= 700) {
		console.log(`Thank you for subscribing to premium.`)		
	} else if (500 <= subs && subs > 700) {
		console.log(`Thank you for subscribing to standard.`)
	} else if (200 <= subs && subs < 500) {
		console.log(`Thank you forsubscribing to basic.`)
	}

*/

/*
	Ternary Operator


	let subs = 550

	subs == 700 ? console.log(`Thank you for subscribing to premium.`) :
	subs == 550 ? console.log(`Thank you for subscribing to standard.`) :
	subs == 340 ? console.log(`Thank you forsubscribing to basic.`)

*/


/*
	Switch Case


	function color () {

	let text = document.getElementById(`space`).value
	let element = document.getElementById(`greet`)

	switch(text){
		case `orange`: element.style.color = `orange`
			break;
		case `pink`: element.style.color = `pink`
			break;
		case `blue`: element.style.color = `blue`
			break;
		case `green`: element.style.color = `green`
			break;
		default: element.style.color = `cyan`
	}
}

*/


/*
	For Loop


	for (let x = 0; x < 20; x++) {
				console.log(`Jumping:`, x, `steps.`);
			}
*/

/*
	Do-While Loop

	let step = 20;

	do {
		console.log(step)
		step--

	} while ( step >= 0 )

*/


/*
	Array

	
	let students = [`Dave`, `Mike`, `Mark`, `Mike`];
	console.log(students.length);
	
	students.unshift(`Vel`);
	console.log(students);

	students.push(`Momoy`);
	console.log(students);


	const last = students.pop();
	console.log(last);

	const beggng = students.shift();
	console.log(beggng);

	let index = students.indexOf(`Dave`);
	console.log(index);

	console.log(students[1]);
	console.log(students[3]);

*/


/*
	Objects


	
	let person = {
		fNm: `Dave`,
		lNm: `Supan`,
		age: 26,
		adds: `Santo Cristo Tarlac City`,
		rfrncs: {
			r1: `Liberte`,
			r2: `Egalite`,
			r3: `Fraternite`
		},
		frnds: {
			f1: `Veni`,
			f2: `Vidi`,
			f3: `Vici`
		},
		admin: true
	};

		function obj() {

			console.log(person[`frnds`]);

		}
		obj()

*/



/*
	Array Destructuring



	function getStudents () {

		return [`Dave`, `Mike`, `Mark`, `Mike`];

	}

	let [w, x,...args] = getStudents();

	console.log(w);
	console.log(x);
	console.log(args);
	

*/



/*
	Object Destructuring


	let display = (person) => console.log(`${person.fNm} ${person.lNm}`);

		let person = {
			fNm: `Dave`,
			lNm: `Supan`,
			age: 26,
			adds: `Santo Cristo Tarlac City`,
	};

	display(person);

*/



/*
	Arrow Function


		let numbers = [4,2,6];
		numbers.sort((a,b) => b - a);
		console.log(numbers);

	
*/
